﻿using Junhee.Utils;
using Mod.Net.Http;
using Mod.Net.OSC;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class PhotoService : MonoBehaviour 
{
	[SerializeField]
	private Photo _photo;
	[SerializeField]
	private QRCodeService _qrCodeServcie;
	[SerializeField]
	private MulticastClient _multicastClient;
	[SerializeField]
	private HttpIn _httpIn;
	[SerializeField]
	private Texture2D _formTexture;

	private void Awake()
	{
		_multicastClient.MessageReceived += OnMessageReceived;
		_httpIn.Received += OnFileReceived;
	}

	private void Start ()
	{
		_photo.GetPhotoInfoDataList(5);
	}
	
	private void Update ()
	{
	}

	private void OnDestroy()
	{
		_multicastClient.MessageReceived -= OnMessageReceived;
		_httpIn.Received -= OnFileReceived;
	}





	private void OnMessageReceived(string message, string argument, string senderId)
	{
		if(message == "RequestPhotoInfo")
		{
			int requestCount = int.Parse(argument);

			string photoInfoString = "";
			List<PhotoInfoData> photoInfoDataList = GetPhotoInfoDataList(requestCount);

			TraceBox.Log("request count: " + argument + " / " + photoInfoDataList.Count);

			for (int i = 0; i < photoInfoDataList.Count; i++)
			{
				string prefix = "|";
				if (i == 0) prefix = "";

				photoInfoString += prefix + photoInfoDataList[i].zoneId + "," + photoInfoDataList[i].index + "," + photoInfoDataList[i].dateString + "," + photoInfoDataList[i].duration;
			}

			_multicastClient.Send("PrintApp", "ResponsePhotoInfo", photoInfoString);
		}
		else if (message == "RequestDelete")
		{
			char[] separator = { '|' };
			string[] arguments = argument.Split(separator);
			Delete(arguments[0], int.Parse(arguments[1]));
		}
	}

	private void OnFileReceived(byte[] rawFileData, Hashtable arguments)
	{
		//TraceBox.Log("raw file data: " + rawFileData.Length + " / " + arguments["zoneId"]);
		string zoneId = arguments["zoneId"].ToString();
		string duration = arguments["duration"].ToString();
		string webpageId = arguments["webpageId"].ToString();
		TraceBox.Log("zoneId: " + zoneId + " / duration: " + duration + " / webpageId: " + webpageId);
		Save(zoneId, rawFileData, "http://sdc2017dex.com/view?id=" + webpageId, duration);

		string h = Format.ConvertDigit(DateTime.Now.Hour.ToString(), 2, "0");
		string m = Format.ConvertDigit(DateTime.Now.Minute.ToString(), 2, "0");
		string s = Format.ConvertDigit(DateTime.Now.Second.ToString(), 2, "0");
		string currentTime = h + " : " + m + " : " + s;
		_multicastClient.Send("PrintApp", "AddPrintInfo", zoneId.ToString() + "|" + GetCurrentPhotoIndex() + "|" + currentTime + "|" + duration);
	}





	public DirectoryInfo GetTodayPhotoDirectoryInfo()
	{
		return _photo.GetTodayDirectoryInfo();
	}

	public List<PhotoInfoData> GetPhotoInfoDataList(int requestCount)
	{
		return _photo.GetPhotoInfoDataList(requestCount);
	}

	public int GetCurrentPhotoIndex()
	{
		return _photo.currentIndex;
	}

	public void Save(string zoneId, byte[] photoByteArray, string qrCodeText, string duraction)
	{
		//byte[] clonedPhotoByteArray = (byte[]) photoByteArray.Clone();

		// qr code 생성
		Debug.Log("qrCodeText: " + qrCodeText);
		Texture2D qrCodeTexture = _qrCodeServcie.GenerateQR(qrCodeText);

		// photoBA를 Texture2D로 변경
		Texture2D photo = new Texture2D(1080, 720, TextureFormat.RGB24, false);
		//photo.LoadRawTextureData(photoByteArray);
		photo.LoadImage(photoByteArray);

		Debug.Log("photo: " + photo);

		// form 그리기
		for (int i = 0; i < _formTexture.width; i++)
		{
			for (int j = 0; j < _formTexture.height; j++)
			{
				Color color = _formTexture.GetPixel(i, j);
				if(color.a != 0)
				{
					photo.SetPixel(i, j, _formTexture.GetPixel(i, j));
				}
			}
		}
		photo.Apply();

		// photo에 qr 그리기
		Vector2 drawStartPos = new Vector2(100, 100);
		for (int i = 25; i < qrCodeTexture.width - 25; i++)
		{
			for (int j = 25; j < qrCodeTexture.height - 25; j++)
			{
				photo.SetPixel(35 + i, 35 + j, qrCodeTexture.GetPixel(i, j));
			}
		}
		photo.Apply();

		// thumbnail 생성
		//Texture2D thumbnail = new Texture2D(1080, 720, TextureFormat.RGB24, false);
		//thumbnail.LoadImage(photoByteArray);
		//thumbnail.Apply();
		//TextureScale.Bilinear(thumbnail, 384, 256);

		_photo.Save(zoneId, photo.EncodeToJPG(), null, duraction);
	}

	public void Delete(string zoneId, int index)
	{
		_photo.Delete(zoneId, index);
	}
}
