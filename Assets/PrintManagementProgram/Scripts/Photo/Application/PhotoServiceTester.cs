﻿using Mod.Net.OSC;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhotoServiceTester : MonoBehaviour 
{
	[SerializeField]
	private PhotoService _photoService;
	[SerializeField]
	private MulticastClient _multicastClient;
	[SerializeField]
	private List<Texture2D> _testImage;

	private void Start ()
	{
		
	}
	
	private void Update ()
	{
		if (Input.GetKeyUp(KeyCode.A))
		{
			Debug.Log("촬영한 이미지 수신 테스트");
			int zoneId = UnityEngine.Random.Range(0, 8);
			_photoService.Save(zoneId.ToString(), _testImage[zoneId].EncodeToJPG(), "http://www.samsung.com/sec/", "22:22:22");

			string currentTime = DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second;
			_multicastClient.Send("PrintApp", "AddPrintInfo", zoneId.ToString() + "|" + _photoService.GetCurrentPhotoIndex() + "|" + currentTime + "|22:22:22");
		}
	}
}
