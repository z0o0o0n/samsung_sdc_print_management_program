﻿using GameDataEditor;
using Junhee.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Photo : MonoBehaviour 
{
	private DirectoryInfo _photoDirectory;
	private DirectoryInfo _todayPhotoDirectoryInfo;
	private string _indexFilePath;
	private string _photoInfoFilePath;
	private GDEPhotoMetaData _photoMetaData;

	public int currentIndex
	{
		get
		{
			return int.Parse(File.ReadAllText(_indexFilePath));
		}
		set
		{
			File.WriteAllText(_indexFilePath, value.ToString());
		}
	}

	private void Awake()
	{
		_photoDirectory = new DirectoryInfo("C:/SDC/Photos");
		if (!Directory.Exists(_photoDirectory.FullName)) Directory.CreateDirectory(_photoDirectory.FullName);

		string todayDirectoryName = "Photos_" + DateTime.Now.Month + "_" + DateTime.Now.Day;
		_todayPhotoDirectoryInfo = new DirectoryInfo(_photoDirectory.FullName + "/" + todayDirectoryName);
		if (!Directory.Exists(_todayPhotoDirectoryInfo.FullName)) Directory.CreateDirectory(_todayPhotoDirectoryInfo.FullName);
	}

	private void Start ()
	{
		_photoMetaData = new GDEPhotoMetaData(GDEItemKeys.PhotoMeta_PhotoMeta);

		_indexFilePath = _todayPhotoDirectoryInfo.FullName + "/index.txt";
		if (!File.Exists(_indexFilePath))
		{
			File.WriteAllText(_indexFilePath, "-1");
			_photoMetaData.ResetAll();
		}
	}
	
	private void Update ()
	{

	}





	private PhotoInfoData ReadPhotoInfo(int index)
	{
		int currentIndex = index;
		string zoneId = null;
		string filename = null;
		string dateString = null;

		for (int i = 0; i < 9; i++)
		{
			string photoFilename = "Photo_" + Format.ConvertDigit(currentIndex.ToString(), 5, "0") + "_" + i.ToString() + ".jpg";
			if (File.Exists(_todayPhotoDirectoryInfo.FullName + "/" + photoFilename))
			{
				zoneId = i.ToString();
				filename = photoFilename;
				break;
			}
		}

		if (String.IsNullOrEmpty(filename)) return null;

		DateTime dateTime = File.GetCreationTime(_todayPhotoDirectoryInfo.FullName + "/" + filename);
		string h = Format.ConvertDigit(dateTime.Hour.ToString(), 2, "0");
		string m = Format.ConvertDigit(dateTime.Minute.ToString(), 2, "0");
		string s = Format.ConvertDigit(dateTime.Second.ToString(), 2, "0");
		dateString = h + " : " + m + " : " + s;

		TraceBox.Log(zoneId + " / " + currentIndex + " / " + dateString);

		PhotoInfoData photoInfoData = new PhotoInfoData(zoneId, currentIndex, dateString, GetMetaData(currentIndex));
		return photoInfoData;
	}

	private void AddMetaData(int photoIndex, string duraction)
	{
		_photoMetaData.indexList.Add(photoIndex);
		_photoMetaData.Set_indexList();

		_photoMetaData.durationList.Add(duraction);
		_photoMetaData.Set_durationList();

		GDEDataManager.Save();

		LogList(_photoMetaData.indexList, "index list: ");
		LogList(_photoMetaData.durationList, "duraction list: ");
	}

	private void DeleteMetaData(int photoIndex)
	{
		int index = -1;
		for (int i = 0; i < _photoMetaData.indexList.Count; i++)
		{
			if (_photoMetaData.indexList[i] == photoIndex)
			{
				index = i;
			}
		}

		if (index == -1) return;

		_photoMetaData.indexList.RemoveAt(index);
		_photoMetaData.Set_indexList();

		_photoMetaData.durationList.RemoveAt(index);
		_photoMetaData.Set_durationList();

		GDEDataManager.Save();

		LogList(_photoMetaData.indexList, "index list: ");
		LogList(_photoMetaData.durationList, "duraction list: ");
	}

	private string GetMetaData(int photoIndex)
	{
		string result = "";
		for (int i = 0; i < _photoMetaData.indexList.Count; i++)
		{
			if (_photoMetaData.indexList[i] == photoIndex)
			{
				result = _photoMetaData.durationList[i].ToString();
			}
		}
		return result;
	}

	private void LogList(List<int> list, string prefix = "")
	{
		string log = prefix;
		for(int i = 0; i < list.Count; i++)
		{
			if(i != 0) log += "," + list[i].ToString();
			else log += list[i].ToString();
		}
		Debug.Log(log);
	}

	private void LogList(List<string> list, string prefix = "")
	{
		string log = prefix;
		for (int i = 0; i < list.Count; i++)
		{
			if (i != 0) log += "," + list[i].ToString();
			else log += list[i].ToString();
		}
		Debug.Log(log);
	}





	public void Save(string zondId, byte[] photoByteArray, byte[] thumbnailByteArray, string duraction)
	{
		//string currentTime = DateTime.Now.Hour + "_" + DateTime.Now.Minute + "_" + DateTime.Now.Second;

		currentIndex += 1;
		string photoFilename = "Photo_" + Format.ConvertDigit(currentIndex.ToString(), 5, "0") + "_" + zondId + ".jpg";
		Debug.Log("photoFilename: " + photoFilename);
		File.WriteAllBytes(_todayPhotoDirectoryInfo.FullName + "/" + photoFilename, photoByteArray);

		//string thumbnailFilename = "Thumb_" + Format.ConvertDigit(currentIndex.ToString(), 5, "0") + ".jpg";
		//File.WriteAllBytes(_todayPhotoDirectoryInfo.FullName + "/" + thumbnailFilename, thumbnailByteArray);

		bool hasMetaData = false;
		for (int i = 0; i < _photoMetaData.indexList.Count; i++)
		{
			if (_photoMetaData.indexList[i] == currentIndex)
			{
				hasMetaData = true;
				_photoMetaData.durationList[i] = duraction;
			}
		}

		if (!hasMetaData)
		{
			AddMetaData(currentIndex, duraction);
		}
	}

	public void Delete(string zondId, int index)
	{
		string photoFilename = "Photo_" + Format.ConvertDigit(index.ToString(), 5, "0") + "_" + zondId + ".jpg";
		File.Delete(_todayPhotoDirectoryInfo.FullName + "/" + photoFilename);
		TraceBox.Log("delete photo / path: " + _todayPhotoDirectoryInfo.FullName + "/" + photoFilename);

		//string thumbnailFilename = "Thumb_" + Format.ConvertDigit(index.ToString(), 5, "0") + ".jpg";
		//File.Delete(_todayPhotoDirectoryInfo.FullName + "/" + thumbnailFilename);
		//TraceBox.Log("delete thumb / path: " + _todayPhotoDirectoryInfo.FullName + "/" + thumbnailFilename);

		DeleteMetaData(index);
	}

	public DirectoryInfo GetTodayDirectoryInfo()
	{
		return _todayPhotoDirectoryInfo;
	}

	public List<PhotoInfoData> GetPhotoInfoDataList(int requestCount)
	{
		List<PhotoInfoData> photoInfoDataList;
		photoInfoDataList = new List<PhotoInfoData>();

		int count = currentIndex;
		while(photoInfoDataList.Count < requestCount)
		{
			//Debug.Log("검색 " + count);
			PhotoInfoData photoInfoData = ReadPhotoInfo(count);
			if(photoInfoData != null)
			{
				//Debug.Log("존재함 " + count);
				photoInfoDataList.Insert(0, photoInfoData);
			}
			else
			{
				//Debug.Log("존재안함. 패스");
			}
			count--;

			if (count < 0)
			{
				//Debug.Log("더이상 파일이 존재하지 않아 검색을 종료함");
				break;
			}
		}

		return photoInfoDataList;
	}

	//public void AddPhotoMetaData(int index, string duration)
	//{
	//	Debug.Log("index list count: " + _photoMetaData.indexList.Count);
	//}
}
