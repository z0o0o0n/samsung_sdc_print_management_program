﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Test : MonoBehaviour 
{
	[SerializeField]
	private Sprite _spriteA;
	[SerializeField]
	private Sprite _spriteB;
	[SerializeField]
	private Image _imageA;
	[SerializeField]
	private Image _imageB;

	private void Start ()
	{
	}
	
	private void Update ()
	{
		if(Input.GetKeyUp(KeyCode.Z))
		{
			//FadeInOut(_image, _spriteB, 0f, 0.5f);
			Dissolve(_imageA, _imageB, _spriteB, 1, 0.4f, OnChanged);
		}
		else if (Input.GetKeyUp(KeyCode.X))
		{
			Dissolve(_imageA, _imageB, _spriteA, 1, 0.4f, OnChanged);
			//FadeInOut(_image, _spriteA, 0f, 0.5f);
		}
	}

	private void OnChanged()
	{
		Debug.Log("Changed");
	}

	private Image _tempImage;
	public void FadeInOut(Image currentImg, Sprite nextSourceImg, float delay = 0f, float duration = 0.5f)
	{
		_tempImage = currentImg;
		currentImg.DOFade(0f, duration/2).SetEase(Ease.OutQuart).OnComplete(()=>OnFadeOut(nextSourceImg, duration/2));

		//Image topLayer = Instantiate(currentImg.gameObject, currentImg.transform).GetComponent<Image>();
		//topLayer.transform.localPosition = Vector3.zero;
		//transitionSeq.Insert(0f, currentImg.DOFade(0f, 0f));

		//transitionSeq.Insert(delay, topLayer.DOFade(0f, duration)).SetEase(Ease.InQuad);
		//transitionSeq.InsertCallback(delay, () => { currentImg.sprite = nextSourceImg; });
		//transitionSeq.Insert(delay, currentImg.DOFade(1f, duration)).SetEase(Ease.OutQuad);

		//transitionSeq.OnComplete(() => ImgaeDissolveComplete(topLayer));
	}

	private void OnFadeOut(Sprite nextSourceImg, float duration)
	{
		_tempImage.sprite = nextSourceImg;
		_tempImage.DOFade(1f, duration).SetEase(Ease.InQuart);
	}


	public void Dissolve(Image imageA, Image imageB, Sprite nextSprite, float delay = 0f, float duration = 0.5f, DG.Tweening.TweenCallback callbackFunction = null)
	{
		Image bufferImage = imageA;
		Image currentImage = imageB;

		if (imageA.color.a == 0)
		{
			bufferImage = imageA;
			currentImage = imageB;
		}
		else if (imageB.color.a == 0)
		{
			bufferImage = imageB;
			currentImage = imageA;
		}

		bufferImage.sprite = nextSprite;
		DOTween.Kill("currentImage.fade." + currentImage.GetInstanceID());
		DOTween.Kill("bufferImage.fade." + bufferImage.GetInstanceID());
		currentImage.DOFade(0f, duration / 2).SetEase(Ease.InSine).SetDelay(delay).SetId("currentImage.fade." + currentImage.GetInstanceID());
		if (callbackFunction != null) bufferImage.DOFade(1f, duration / 2).SetEase(Ease.OutSine).SetDelay(delay).SetId("bufferImage.fade." + bufferImage.GetInstanceID()).OnComplete(callbackFunction);
		else bufferImage.DOFade(1f, duration / 2).SetEase(Ease.OutSine).SetDelay(delay).SetId("bufferImage.fade." + bufferImage.GetInstanceID());
	}
}
