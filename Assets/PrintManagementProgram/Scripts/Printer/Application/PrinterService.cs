﻿using Mod.Net.OSC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrinterService : MonoBehaviour 
{
	[SerializeField]
	private Printer _printer;
	[SerializeField]
	private QRCodeService _qrCodeService;
	[SerializeField]
	private PhotoService _photoService;
	[SerializeField]
	private MulticastClient _multicastClient;

	private void Start ()
	{
		_multicastClient.MessageReceived += OnMessageReceived;
	}
	
	private void Update ()
	{
		if (Input.GetKeyUp(KeyCode.P))
		{
			Print("Photo_00006_1.jpg");
		}
	}

	private void OnDestroy()
	{
		_multicastClient.MessageReceived -= OnMessageReceived;
	}





	private void OnMessageReceived(string message, string argument, string senderId)
	{
		if(message == "RequestPrint")
		{
			string filename = argument;
			Print(filename);
		}
	}





	public void Print(string printFilename)
	{
		string imageFilePath = _photoService.GetTodayPhotoDirectoryInfo().FullName + "/" + printFilename;
		_printer.Print(imageFilePath);
	}
}
