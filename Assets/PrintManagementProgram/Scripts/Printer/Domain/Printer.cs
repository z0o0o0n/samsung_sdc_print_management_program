﻿using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using ZXing;
using ZXing.QrCode;

public class Printer : MonoBehaviour 
{
	private string _imageFilePath;
	private PrintDocument _printDocument;

	private void Start ()
	{
		CreatePrintDocument();
	}
	
	private void Update ()
	{
	}





	private void OnPrintPage(object sender, PrintPageEventArgs eventArgs)
	{
		Debug.Log("print Page");
		TraceBox.Log("print Page");

		if (string.IsNullOrEmpty(_imageFilePath))
		{
			TraceBox.Log("print filename is null or empty");
			return;
		}
		
		System.Drawing.Image photo = System.Drawing.Image.FromFile(_imageFilePath);

		//eventArgs.Graphics.PageScale = 0.5f;
		eventArgs.Graphics.DrawImage(photo, new Rectangle(8, 5, 600, 400), new Rectangle(0, 0, 1080, 720), GraphicsUnit.Pixel);

		photo.Dispose();
		//eventArgs.HasMorePages = false;
	}

	private void OnBeginPrint(object sender, PrintEventArgs eventArgs)
	{
		//TraceBox.Log("Beain Print");
	}

	private void OnEndPrint(object sender, PrintEventArgs eventArgs)
	{
		//TraceBox.Log("End Print");
	}





	private void CreatePrintDocument()
	{
		_printDocument = new PrintDocument();
		_printDocument.PrintPage += OnPrintPage;
		_printDocument.BeginPrint += OnBeginPrint;
		_printDocument.EndPrint += OnEndPrint;
		_printDocument.PrintController = new StandardPrintController();
		_printDocument.DefaultPageSettings.Landscape = true;
		_printDocument.DefaultPageSettings.PrinterResolution.Kind = PrinterResolutionKind.High;
	}





	public void Print(string imageFilePath)
	{
		Debug.Log("print / path: " + imageFilePath);
		TraceBox.Log("print / path: " + imageFilePath);

		_imageFilePath = imageFilePath;

		if (!File.Exists(_imageFilePath))
		{
			TraceBox.Log("image file is null");
			return;
		}

		try
		{
			_printDocument.Print();
		}
		catch (System.Exception exception)
		{
			TraceBox.Log("print error: " + exception);
		}
	}
}
