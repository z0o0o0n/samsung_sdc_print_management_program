﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ZXing;
using ZXing.QrCode;

public class QRCode : MonoBehaviour 
{
	private Texture2D _qrTexture;
	private Color32[] _color32;

	private void Start ()
	{
		_qrTexture = new Texture2D(256, 256);
	}
	
	private void Update ()
	{
		
	}





	private Color32[] Encode(string textForEncoding, int width, int height)
	{
		BarcodeWriter barcodeWriter = new BarcodeWriter();
		barcodeWriter.Format = BarcodeFormat.QR_CODE;
		barcodeWriter.Options = new QrCodeEncodingOptions();
		barcodeWriter.Options.Width = width;
		barcodeWriter.Options.Height = height;
		Color32[] result = barcodeWriter.Write(textForEncoding);
		
		return result;
	}

	public Texture2D GenerateQR(string text)
	{
		_color32 = Encode(text, _qrTexture.width, _qrTexture.height);
		_qrTexture.SetPixels32(_color32);
		_qrTexture.Apply();
		return _qrTexture;
	}
}
