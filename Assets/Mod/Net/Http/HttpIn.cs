﻿namespace Mod.Net.Http
{
	using System.Text;
	using UnityEngine;
	using System.Collections;
	using MOD.Net.Http;
	using System.Net;
	using UnityEngine.UI;
	using System.IO;

	public class HttpIn : HTTPPage
	{
		public delegate void HttpInEvent(byte[] file, Hashtable arguments);
		public event HttpInEvent Received;

		private bool _isDone = false;
		private byte[] _file;
		private Hashtable _hash;

		void Awake()
		{
		}

		private void Update()
		{
			if (_isDone)
			{
				_isDone = false;

				//_texture.LoadImage(_file);
				//CanvasRenderer renderer = _image.GetComponent<CanvasRenderer>();
				//renderer.SetTexture(_texture);

				//File.WriteAllBytes(Application.streamingAssetsPath + "/SPen/test.png", _file);

				Debug.Log("http in is done");
				if (Received != null) Received(_file, _hash);
			}
		}


		public override void Get(Hashtable hash, HttpListenerContext context)
		{
			if (context.Request.QueryString.HasKeys())
			{
				int status = 200;
				string message = "ok";

				// 원하는 값으로 응답 할 때
				string str = "success";
				HttpServer.WriteResponse(context, status, message, Encoding.UTF8.GetBytes(str));

				// message와 status로 응답할때
				//HttpServer.WriteResponse(context, status, message, null);

				// 원하는 Html 파일로 응답 할 때
				//HttpServer.WriteResponse(context, status, message, HttpServer.GetFile("c:\\index.html"));
			}
		}

		public override void Post(Hashtable hash, byte[] file, HttpListenerContext context)
		{
			int status = 200;
			string message = "ok";

			// 원하는 값으로 응답 할 때
			string str = "Received !!";
			HttpServer.WriteResponse(context, status, message, Encoding.UTF8.GetBytes(str));

			// message와 status로 응답할때
			//HttpServer.WriteResponse(context, status, message, null);

			// 원하는 Html 파일로 응답 할 때
			//HttpServer.WriteResponse(context, status, message, HttpServer.GetFile("c:\\index.html"));

			if (file != null)
			{
				_file = file;
				_hash = hash;
				//Debug.Log("file.Count: " + file.Length);
				//File.WriteAllBytes("test.png", file);
				_isDone = true;
			}
		}
	}
}