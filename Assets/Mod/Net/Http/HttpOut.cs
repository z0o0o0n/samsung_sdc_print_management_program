﻿namespace MOD.Net.Http
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;

	public class HttpOut : MonoBehaviour
	{
		void Start()
		{
		}

		void Update()
		{
		}




		public void Send(string ip, int port, byte[] file, Hashtable argument, string page)
		{
			TraceBox.Log("http out: " + ip + " / " + port + " / " + page);
			StartCoroutine(SendFile(ip, port, file, argument, page));
		}

		private IEnumerator SendFile(string ip, int port, byte[] file, Hashtable argument, string page)
		{
			string url = "http://" + ip + ":" + port.ToString() + "/" + page;

			WWWForm form = new WWWForm();
			form.headers["Content-Type"] = "multipart/form-data";

			foreach (DictionaryEntry entry in argument)
			{
				form.AddField(entry.Key.ToString(), entry.Value.ToString());
			}
			
			if (file != null) form.AddBinaryData("file", file);

			WWW www = new WWW(url, form);
			yield return www;

			if (!string.IsNullOrEmpty(www.error))
			{
				Debug.Log("http out error: " + www.error);
				TraceBox.Log("http out error: " + www.error);
			}
			else if (www.isDone)
			{
				www.Dispose();
				Debug.Log("http out is done");
				TraceBox.Log("http out is done");
			}
		}
	}
}