﻿namespace Mod.Net.OSC
{
	using DG.Tweening;
	using System.Collections.Generic;
	using UnityEngine;

	public class MulticastClient : MonoBehaviour
	{
		public delegate void MulticastClientEvent(string message, string argument, string senderId);
		public event MulticastClientEvent MessageReceived;

		[SerializeField]
		private string _clientId;
		[SerializeField]
		private OscIn _oscIn;
		[SerializeField]
		private OscOut _oscOut;
		[SerializeField]
		private bool _isAutoConnection = false;
		private int inPort = 10084;
		private int outPort = 10084;
		private List<MulticastClientData> _clientDataList = new List<MulticastClientData>();

		public OscIn oscIn
		{
			get { return _oscIn; }
		}





		private void Awake()
		{
			Application.runInBackground = true;
			Init();
		}
		private void Start()
		{

			//Connect();
		}

		private void Update()
		{
			//if (Input.GetMouseButtonUp(0))
			//{
			//	Connect();
			//}
			//if (Input.GetKeyUp(KeyCode.Z))
			//{
			//	Connect();
			//}
		}

		private void OnDestroy()
		{
			_oscIn.Unmap(OnClientConnection);
			_oscIn.Unmap(OnClientConnectionResponse);
			_oscIn.Unmap(OnMessageReception);
			_oscIn.Close();
			if (_oscOut) _oscOut.Close();
		}





		private void OnClientConnection(OscMessage message)
		{
			string receiverId;
			string ip;
			string port;
			string senderId;
			message.TryGet(0, out receiverId);
			message.TryGet(1, out ip);
			message.TryGet(2, out port);
			message.TryGet(3, out senderId);

			if (receiverId == "others")
			{
				if (senderId == _clientId)
				{
					//TraceBox.Log("on client connection / it's me! return.");
					return;
				}
			}

			if (receiverId == "others" || receiverId == "all")
			{
				if (HasClient(senderId))
				{
					TraceBox.Log("클라이언트 존재함 / id: " + senderId + " / ip: " + ip + " / port: " + port);
				}
				else
				{
					TraceBox.Log("클라이언트 추가함 / id: " + senderId + " / ip: " + ip + " / port: " + port);
					MulticastClientData clientData = new MulticastClientData(senderId, ip, int.Parse(port));
					_clientDataList.Add(clientData);
				}

				LogClient();
				SendConnectionResponse();
			}
		}

		private void OnClientConnectionResponse(OscMessage message)
		{
			string receiverId;
			string ip;
			string port;
			string senderId;
			message.TryGet(0, out receiverId);
			message.TryGet(1, out ip);
			message.TryGet(2, out port);
			message.TryGet(3, out senderId);

			if (receiverId == "others")
			{
				if (senderId == _clientId)
				{
					//TraceBox.Log("on client connection response / it's me! return.");
					return;
				}
			}

			if (receiverId == "others" || receiverId == "all")
			{
				if (HasClient(senderId))
				{
					TraceBox.Log("클라이언트 존재함 / id: " + senderId + " / ip: " + ip + " / port: " + port);
				}
				else
				{
					TraceBox.Log("클라이언트 추가함 / id: " + senderId + " / ip: " + ip + " / port: " + port);
					MulticastClientData clientData = new MulticastClientData(senderId, ip, int.Parse(port));
					_clientDataList.Add(clientData);
				}
				LogClient();
			}
		}

		private void OnMessageReception(OscMessage message)
		{
			string receiverId;
			string messageString;
			string argument;
			string senderId;
			message.TryGet(0, out receiverId);
			message.TryGet(1, out messageString);
			message.TryGet(2, out argument);
			message.TryGet(3, out senderId);

			if (_clientId != receiverId) return;

			if (MessageReceived != null) MessageReceived(messageString, argument, senderId);

			TraceBox.Log("메시지 수신 / senderId: " + senderId + " / message: " + messageString + " / argument: " + argument);
		}






		private void Init()
		{
			_oscIn.Map("/clientConnection", OnClientConnection);
			_oscIn.Map("/clientConnectionResponse", OnClientConnectionResponse);
			_oscIn.Map("/message", OnMessageReception);
			_oscIn.Open(inPort, "224.1.1.84");

			if (_oscOut) _oscOut.Open(outPort, "224.1.1.84");

			if (_isAutoConnection) Connect();
		}

		private void SendConnectionResponse()
		{
			OscMessage message = new OscMessage("/clientConnectionResponse");
			message.Add("others");
			message.Add(Network.player.ipAddress);
			message.Add(inPort.ToString());
			message.Add(_clientId);
			if(_oscOut) _oscOut.Send(message);

			TraceBox.Log("연결 알림 / receiver: others / ip: " + Network.player.ipAddress + " / port: " + inPort + " / clientId: " + _clientId);
		}

		private bool HasClient(string id)
		{
			TraceBox.Log("has client");
			bool result = false;
			for (int i = 0; i < _clientDataList.Count; i++)
			{
				TraceBox.Log(_clientDataList[i].id + " == " + id);
				if (_clientDataList[i].id == id)
				{
					result = true;
					break;
				}
				else
				{
					result = false;
				}
			}
			return result;
		}

		private void LogClient()
		{
			string log = "clients [ ";
			for (int i = 0; i < _clientDataList.Count; i++)
			{
				if (i == 0) log += _clientDataList[i].id;
				else log += ", " + _clientDataList[i].id;
			}
			log += "]";
			TraceBox.Log(log);
		}





		public void Connect()
		{
			OscMessage message = new OscMessage("/clientConnection");
			message.Add("others");
			message.Add(Network.player.ipAddress);
			message.Add(inPort.ToString());
			message.Add(_clientId);
			if(_oscOut) _oscOut.Send(message);

			TraceBox.Log("연결 알림 / receiver: others / ip: " + Network.player.ipAddress + " / port: " + inPort + " / clientId: " + _clientId);
		}

		public void Send(string receiverId, string messageString, string argument = "")
		{
			OscMessage message = new OscMessage("/message");
			message.Add(receiverId);
			message.Add(messageString);
			message.Add(argument);
			message.Add(_clientId);
			if(_oscOut) _oscOut.Send(message);

			TraceBox.Log("메시지 전송 / receiverId: " + receiverId + " / message: " + messageString + " / senderId: " + _clientId);
		}

		public MulticastClientData GetClientData(string clientId)
		{
			MulticastClientData clientData = null;
			for(int i = 0; i < _clientDataList.Count; i++)
			{
				if (_clientDataList[i].id == clientId) clientData = _clientDataList[i];
			}
			return clientData;
		}
	}
}