﻿namespace Mod.Net.OSC
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;

	public class MulticastClientData
	{
		private string _id;
		private string _ip;
		private int _port;

		public MulticastClientData(string id = "", string ip = "", int port = 8000)
		{
			_id = id;
			_ip = ip;
			_port = port;
		}

		public string id
		{
			get { return _id; }
		}

		public string ip
		{
			get { return _ip; }
		}

		public int port
		{
			get { return _port; }
		}
	}
}